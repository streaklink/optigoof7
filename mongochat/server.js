const mongo = require("mongodb").MongoClient;
const port = process.env.PORT;
const client = require("socket.io").listen(port).sockets;
// Connect to mongo
var uri =
  "mongodb+srv://optigoo:maxmaya007@clusteroptigo-aut6y.mongodb.net/optigoo?retryWrites=true&w=majorityJWT_KEY=WinterIsComingGOT2019";
mongo.connect(uri, function (err, modb) {
  // mongo.connect('mongodb://127.0.0.1/mongochat', function(err, db){
  if (err) {
    throw err;
  }
  console.log("MongoDB connected...");
  // Connect to Socket.io
  client.on("connection", function (socket) {
    let chat = modb.db("optigoo").collection("chats");
    // Create function to send status
    sendStatus = function (s) {
      socket.emit("status", s);
    };
    // Get chats from mongo collection
    socket.on("show", function (data) {
      console.log(data);
      chat
        .find({ senderId: data.senderId })
        .limit(100)
        .sort({ _id: 1 })
        .toArray(function (err, res) {
          if (err) {
            throw err;
          }
          // Emit the messages
          socket.emit("output", res);
        });
    });
    // Handle input events
    socket.on("input", function (data) {
      let name = data.name;
      let message = data.message;
      let senderIdx = data.senderId;
      let receiverIdx = data.receiverId;
      let dt = data.date;
      console.log(dt);
      console.log(receiverIdx);
      // Check for name and message
      if (name == "" || message == "" || senderIdx == "" || receiverIdx == "" || dt == "" ) {
        // Send error status
        sendStatus("Please enter a name and message");
      } else {
        // Insert message
        chat.insert(
          {
            name: name,
            message: message,
            senderId: senderIdx,
            userId: receiverIdx,
            date: dt 
          },
          function () {
            // client.emit('output', [data]);
            client.emit("output", [
              {
                name: data.name,
                message: data.message,
                userId: data.receiverId,
                senderId: data.senderId,
                date: data.date
              },
            ]);
            // Send status object
            sendStatus({
              message: "Message sent",
              clear: true,
            });
          }
        );
      }
    });
    // Handle clear
    socket.on("clear", function (data) {
      // Remove all chats from collection
      chat.remove({}, function () {
        // Emit cleared
        socket.emit("cleared");
      });
    });
  });
});
