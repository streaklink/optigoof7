// Import Vue
import Vue from 'vue';

// Import Framework7
import Framework7 from './framework7-custom.js';

// Import Framework7-Vue Plugin
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js';

// Import Framework7 Styles
import '../css/framework7-custom.less';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.less';


// Import App Component
import App from '../components/app.vue';

// import socketio from 'socket.io';
// import VueSocketIO from 'vue-socket.io';
//import io from 'socket.io-client';

// export const SocketInstance = socketio('http://optigoof7-api.herokuapp.com:3001');
//const socket = io('http://127.0.0.1:4000');

// Vue.use(VueSocketIO, SocketInstance);

// Init Framework7-Vue Plugin
Framework7.use(Framework7Vue);

// Init App
new Vue({
  el: '#app',
  render: (h) => h(App),

  // Register App Component
  components: {
    app: App,
    
  },
});

