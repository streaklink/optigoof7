import LoginPage from '../pages/login.vue';
import OtpPage from '../pages/otp.vue';
import DashboardPage from '../pages/dashboard.vue';
import ChatPage from '../pages/chat.vue';
import ProfilePage from '../pages/profile.vue';
//categories and product
import CategoriesPage from '../pages/categories.vue';
import ProductListPage from '../pages/product-list.vue';
import ProductDetailsPage from '../pages/product-details.vue';
import HistoryViewPage from '../pages/history-view.vue';
import FavoritePage from '../pages/favorite.vue';
import SearchPage from '../pages/search.vue';
//tabs chat
import ChatTabPage from '../pages/tabs/chatTab.vue';
import GroupTabPage from '../pages/tabs/groupTab.vue';
import FriendsTabPage from '../pages/tabs/friendsTab.vue';
import ChatDataPage from '../pages/tabs/chatData.vue';
import SearchFriendPage from '../pages/tabs/search-friend.vue';
import FriendsProfilePage from '../pages/tabs/friends-profile.vue';
import FriendHistory from '../pages/friends-history.vue';

import FormPage from '../pages/form.vue';
import LeftPage1 from '../pages/left-page-1.vue';
import LeftPage2 from '../pages/left-page-2.vue';
import DynamicRoutePage from '../pages/dynamic-route.vue';
import RequestAndLoad from '../pages/request-and-load.vue';
import NotFoundPage from '../pages/404.vue';

var routes = [
  {
    path: '/',
    async(routeTo, routeFrom, resolve, reject) {
      var token = window.localStorage.getItem("token");
      //var userIsLoggedIn = 'false';
    if (token != null) {
      resolve({
        component: DashboardPage,
      });
    } else {
      resolve({
        component: LoginPage,
        });
      }
    },
  },
  {
    path: '/otp/:phone',
    component: OtpPage,
  },
  //product
  {
    path: '/categories/:category',
    component: CategoriesPage,
  },
  {
    path: '/product-list/:category',
    component: ProductListPage,
  },
  {
    path: '/product-details/:pddata',
    component: ProductDetailsPage,
  },
  {
    path: '/history-view/',
    component: HistoryViewPage,
  },
  {
    path: '/favorite/',
    component: FavoritePage,
  },
  //chat
  {
    path: '/chat/',
    component: ChatPage,
    tabs: [
      {
        path: '/',
        id: 'chatID',
        component: ChatTabPage,
      },
      {
        path: '/group/',
        id: 'groupID',
        component: GroupTabPage,
      },
      {
        path: '/friends/',
        id: 'friendsID',
        component: FriendsTabPage,
      },
    ],
  },
  {
    path: '/chatdata/',
    component: ChatDataPage,
  },
  {
    path: '/search-friend/',
    component: SearchFriendPage,
  },
  {
    path: '/friends-history/:phone',
    component: FriendHistory,
  },
  {
    path: '/friends-profile/',
    component: FriendsProfilePage,
  },
  {
    path: '/profile/',
    component: ProfilePage,
  },
  {
    path: '/search/:searchData',
    component: SearchPage,
  },
  {
    path: '/left-page-1/',
    component: LeftPage1,
  },
  {
    path: '/left-page-2/',
    component: LeftPage2,
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            context: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
